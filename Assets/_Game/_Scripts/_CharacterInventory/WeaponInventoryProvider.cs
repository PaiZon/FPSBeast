using System;
using System.Collections.Generic;
using DG.Tweening;
using Game.Scripts.Input;
using Game.Scripts.Weapon;
using UnityEngine;
using UnityEngine.InputSystem;
using InputActionType = Game.Scripts.Input.InputActionType;

namespace Game.Scripts.Inventory
{
    public class WeaponInventoryProvider
    {
        private readonly List<IWeapon> _weapons = new List<IWeapon>();
        private readonly Vector3 _equipStartPosition = new Vector3(90f, 0f, 0f);
        
        private int _currentWeaponID;

        public WeaponInventoryProvider()
        {
            _currentWeaponID = 0;
        }

        public void Tick()
        {
            _weapons[_currentWeaponID].Tick();
        }
        
        public void Add(IWeapon weapon)
        {
            _weapons.Add(weapon);
            weapon.GetTransform().gameObject.SetActive(false);
        }

        public void SwitchWeapon(InputAction.CallbackContext callbackContext)
        {
            var scrollDirection = callbackContext.ReadValue<float>();
            scrollDirection = Math.Clamp(scrollDirection, -1, 1);
            HideCurrentWeapon();
            _currentWeaponID += (int)scrollDirection;
            _currentWeaponID = _currentWeaponID > _weapons.Count - 1 ? 0 : _currentWeaponID;
            _currentWeaponID = _currentWeaponID < 0 ? _weapons.Count - 1 : _currentWeaponID;
            EquipWeapon(_currentWeaponID);
        }

        private void HideCurrentWeapon()
        {
            var id = _currentWeaponID;
            var weaponTransform = _weapons[id].GetTransform();
            weaponTransform.gameObject.SetActive(false);
        }

        public void EquipWeapon(int weaponIndex)
        {
            var weapon = _weapons[weaponIndex].GetTransform();
            weapon.transform.localEulerAngles = _equipStartPosition;
            weapon.gameObject.SetActive(true);
            weapon.DOLocalRotate(Vector3.zero, 0.2f);
        }
    }
}