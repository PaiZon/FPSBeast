using UnityEngine;

public interface IGroundChecker
{
    bool IsGrounded();
}

public class SphereGroundChecker : IGroundChecker
{
    private readonly Transform _groundCheckPosition;
    private readonly float _radius;

    public SphereGroundChecker(Transform groundCheckPosition, float radius)
    {
        _groundCheckPosition = groundCheckPosition;
        _radius = radius;
    }
    
    public bool IsGrounded()
    {
        return Physics.CheckSphere(_groundCheckPosition.localPosition, _radius);
    }
}
