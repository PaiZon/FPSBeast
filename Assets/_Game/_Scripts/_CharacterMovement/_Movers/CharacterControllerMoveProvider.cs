﻿using UnityEngine;

namespace Game.Scripts.CharacterMovement
{
    public class CharacterControllerMoveProvider : IObjectMoveProvider
    {
        private readonly CharacterController _characterController;
        public CharacterControllerMoveProvider(CharacterController characterController)
        {
            _characterController = characterController;
        }
        public void Move(Vector3 motion)
        {
            _characterController.Move(motion);
        }
    }
}