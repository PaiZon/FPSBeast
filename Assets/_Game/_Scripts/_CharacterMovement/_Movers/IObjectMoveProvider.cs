﻿using UnityEngine;

namespace Game.Scripts.CharacterMovement
{
    public interface IObjectMoveProvider
    {
        void Move(Vector3 motion);
    }
}