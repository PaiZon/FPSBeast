﻿using Cinemachine;
using Game.Scripts.Infrastructure.Servisces;
using Game.Scripts.Input;
using Game.Scripts.Inventory;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using InputActionType = Game.Scripts.Input.InputActionType;

namespace Game.Scripts.CharacterMovement
{
    [RequireComponent(typeof(CharacterController))]
    public class CharacterMovement : MonoBehaviour, IService
    {
        private const float GRAVITY = -9.81f;
        
        [SerializeField, FoldoutGroup("Aim Settings", true)] private CinemachineVirtualCamera _cinemachineVirtualCamera;
        [SerializeField, FoldoutGroup("Movement Settings", true)] private float _speed = 1.0f;
        [SerializeField, Range(0f, 1f), FoldoutGroup("Aim Settings", true)] private float _sensitivity;
        [SerializeField, Range(0f, 90f), FoldoutGroup("Aim Settings", true)] private float _verticalClampAngle;
        [SerializeField, FoldoutGroup("Ground Check", true)] private Transform _groundCheckTransform;
        [SerializeField, Range(0f,1f), FoldoutGroup("Ground Check", true)] private float _groundCheckRadius;
        
        [ShowInInspector, ReadOnly, FoldoutGroup("Aim Settings", true)] private IAimProvider _aimProvider;
        [ShowInInspector, ReadOnly, FoldoutGroup("Movement Settings", true)] private IObjectMoveProvider _objectMoveProvider;
        [ShowInInspector, ReadOnly, FoldoutGroup("Movement Settings", true)] private CharacterController _characterController;
        [ShowInInspector, ReadOnly, FoldoutGroup("Movement Settings", true)] private Vector3 _inputVector;
        [ShowInInspector, ReadOnly, FoldoutGroup("Movement Settings", true)] private bool _shouldMove;
        [ShowInInspector, ReadOnly, FoldoutGroup("Ground Check", true)] private IGroundChecker _groundChecker;
        
        private IInputService _inputService;
        private WeaponInventoryProvider _weaponInventoryProvider;
        private float _yVelocity;
        
        public void Initialize(WeaponInventoryProvider weaponInventoryProvider)
        {
            _inputService = Services.GetSingle<IInputService>();
            _characterController = GetComponent<CharacterController>();
            _weaponInventoryProvider = weaponInventoryProvider;
            _aimProvider = new AimCharacter(_cinemachineVirtualCamera, _sensitivity, _verticalClampAngle, transform);
            _objectMoveProvider = new CharacterControllerMoveProvider(_characterController);
            _groundChecker = new SphereGroundChecker(_groundCheckTransform, _groundCheckRadius);
            SubscribeToInput();
            _cinemachineVirtualCamera.Follow = transform;
        }

        private void SubscribeToInput()
        {
            _inputService.AddListener(InputActionType.WASDMovementPerformed, GetInputVector);
            _inputService.AddListener(InputActionType.WASDMovementCanceled, GetInputVector);
            _inputService.AddListener(InputActionType.MouseMovePerformed, _aimProvider.Aim);
            _inputService.AddListener(InputActionType.MouseWheelPerformed, _weaponInventoryProvider.SwitchWeapon);
        }

        public void Update()
        {
            _weaponInventoryProvider.Tick();
        }

        public void FixedUpdate()
        {
            _yVelocity += GRAVITY * Time.deltaTime;
            if (_groundChecker.IsGrounded())
                _yVelocity = GRAVITY;
            _characterController.Move(Vector3.up * _yVelocity * Time.deltaTime);
            if(_shouldMove == false)
                return;
            var direction = transform.right * _inputVector.x + transform.forward * _inputVector.z;
            direction.y = _yVelocity;
            _objectMoveProvider.Move(direction * _speed * Time.fixedDeltaTime);
        }

        private void GetInputVector(InputAction.CallbackContext obj)
        {
            _shouldMove = obj.phase == InputActionPhase.Performed;
            _inputVector = new Vector3(obj.ReadValue<Vector2>().x, 0f, obj.ReadValue<Vector2>().y);
        }
    }
}