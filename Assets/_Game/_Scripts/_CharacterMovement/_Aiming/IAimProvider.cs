﻿using UnityEngine.InputSystem;

namespace Game.Scripts.CharacterMovement
{
    public interface IAimProvider
    {
        void Aim(InputAction.CallbackContext obj);
    }
}