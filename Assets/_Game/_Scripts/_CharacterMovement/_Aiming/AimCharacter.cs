﻿using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Scripts.CharacterMovement
{
    public class AimCharacter : IAimProvider
    {
        private readonly CinemachineVirtualCamera _cinemachineVirtualCamera;
        private readonly Transform _playerTransform;

        private readonly float _sensitivity;
        private readonly float _verticalClampAngle;

        private float _xRotation;

        public AimCharacter(CinemachineVirtualCamera cinemachineVirtualCamera, float sensitivity, float verticalClampAngle, Transform playerTransform)
        {
            _cinemachineVirtualCamera = cinemachineVirtualCamera;
            _sensitivity = sensitivity;
            _verticalClampAngle = verticalClampAngle;
            _playerTransform = playerTransform;
        }
        
        public void Aim(InputAction.CallbackContext obj)
        {
            var mouseDelta = obj.ReadValue<Vector2>() * _sensitivity;
            _xRotation -= mouseDelta.y;
            _xRotation = Mathf.Clamp(_xRotation, -_verticalClampAngle, _verticalClampAngle);
            _playerTransform.Rotate(Vector3.up * mouseDelta.x);
            _cinemachineVirtualCamera.transform.localRotation = Quaternion.Euler(_xRotation, 0f, 0f);
        }
    }
}