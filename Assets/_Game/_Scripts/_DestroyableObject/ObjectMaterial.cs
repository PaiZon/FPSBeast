﻿using UnityEngine;

namespace Game.Scripts.DestroyableObject
{
    [CreateAssetMenu(menuName = "FPS/Object Material", fileName = "new Object Material")]
    public class ObjectMaterial : ScriptableObject
    {
        [SerializeField] private int _durability;
        public int Durability => _durability;
    }
}