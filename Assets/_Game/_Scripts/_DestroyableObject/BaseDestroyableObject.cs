﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Scripts.DestroyableObject
{
    [RequireComponent(typeof(Rigidbody))]
    public class BaseDestroyableObject : MonoBehaviour, IDestroyableObject
    {
        [SerializeField] private ObjectMaterial _objectMaterial;
        [ShowInInspector, ReadOnly] private int _currentDurability;
        
        [SerializeField, FoldoutGroup("Events")] private UnityEvent HitReceivedEvent;
        [SerializeField, FoldoutGroup("Events")] private UnityEvent ObjectDestroyedEvent;
        
        private void OnEnable()
        {
            _currentDurability = _objectMaterial.Durability;
        }

        public void GetHit(int damage)
        {
            _currentDurability -= damage;
            HitReceivedEvent?.Invoke();
            if (_currentDurability <= 0)
                DisableObject();
        }

        public ObjectMaterial GetObjectMaterial()
        {
            return _objectMaterial;
        }

        private void DisableObject()
        {
            ObjectDestroyedEvent?.Invoke();
        }
    }
}