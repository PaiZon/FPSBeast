namespace Game.Scripts.DestroyableObject
{
    public interface IDestroyableObject
    {
        void GetHit(int damage);
        ObjectMaterial GetObjectMaterial();
    }
}
