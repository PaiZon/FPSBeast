using DG.Tweening;
using UnityEngine;

public class SlideBehaviour : MonoBehaviour
{
    [SerializeField, Range(0f, 0.4f)] private float _returnTime;
    [SerializeField] private float _localKickbackPosition = -0.1f;
    private readonly float _originalValue = -0.1387274f;

    public void PlayShootAnimation()
    {
        var shotSequence = DOTween.Sequence();
        shotSequence.Append(transform.DOLocalMoveZ(_localKickbackPosition, _returnTime * 0.5f));
        shotSequence.Append(transform.DOLocalMoveZ(_originalValue, _returnTime * 0.5f));
    }

}
