﻿namespace Game.Scripts.Input
{
    public enum InputActionType
    {
        WASDMovementPerformed,
        MouseMoveStarted,
        LeftMouseButtonStarted,
        LeftMouseButtonCanceled,
        RButtonStarted,
        Alpha1Started,
        Alpha2Started,
        Alpha3Started,
        MouseWheelPerformed,
        WASDMovementCanceled,
        MouseMovePerformed
    }
}