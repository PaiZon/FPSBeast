namespace Game.Infrastructure.StateMachine
{
    public class BaseState : IBaseState
    {
        public BaseState()
        {
            
        }
        
        public virtual void Enter()
        {

        }

        public virtual void Tick()
        {

        }

        public virtual void FixedTick()
        {

        }

        public virtual void Exit()
        {

        }
    }
}