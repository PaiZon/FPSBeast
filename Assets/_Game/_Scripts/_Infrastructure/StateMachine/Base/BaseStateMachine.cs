using System;
using System.Collections.Generic;
using Game.Scripts.Infrastructure.Servisces;
using UnityEngine;

namespace Game.Infrastructure.StateMachine
{
    public class BaseStateMachine : IStateMachine, IService
    {
        private readonly Dictionary<Type, IBaseState> _states = new Dictionary<Type, IBaseState>();
        private IBaseState _currentState;


        public void AddState<TState>(IBaseState instance) where TState : IBaseState
        {
            if(_states.ContainsKey(typeof(TState)) == false)
                _states.Add(typeof(TState), instance);
        }

        public void Tick()
        {
            _currentState?.Tick();
        }

        public void FixedTick()
        {
            _currentState?.FixedTick();
        }

        public void ChangeStateTo<TState>() where TState : IBaseState
        {
            _currentState?.Exit();
            _currentState = GetState<TState>();
            _currentState?.Enter();
        }

        private IBaseState GetState<TState>() where TState : IBaseState
        {
            if(_states.ContainsKey(typeof(TState)))
                return _states[typeof(TState)];

            Debug.LogWarning($"No State registered {typeof(TState)}");
            return null;
        }
    }
}