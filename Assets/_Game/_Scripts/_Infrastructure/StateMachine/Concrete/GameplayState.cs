﻿using Game.Scripts.CharacterMovement;
using Game.Scripts.Infrastructure.Servisces;
using Game.Scripts.Input;
using UnityEngine;

namespace Game.Infrastructure.StateMachine
{
    public class GameplayState : BaseState
    {
        private IInputService _playerInputService;
        
        
        public override void Enter()
        {
            base.Enter();
            Cursor.lockState = CursorLockMode.Locked;
            _playerInputService = Services.GetSingle<IInputService>();
            _playerInputService.SetInputSystemState(true);
        }
    }
}