namespace Game.Infrastructure.StateMachine
{
    public interface IStateMachine
    {
        void AddState<TState>(IBaseState instance) where TState : IBaseState; 
        void Tick();
        void FixedTick();
        void ChangeStateTo<TState>() where TState : IBaseState;
    }

}