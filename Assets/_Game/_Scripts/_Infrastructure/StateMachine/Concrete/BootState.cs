﻿using System.Collections.Generic;
using Game.Scripts.CharacterMovement;
using Game.Scripts.Infrastructure.Servisces;
using Game.Scripts.Input;
using Game.Scripts.Inventory;
using Game.Scripts.Weapon;
using UnityEngine;

namespace Game.Infrastructure.StateMachine
{
    public class BootState : BaseState
    {
        private readonly CharacterMovement _characterMovement;
        private readonly Transform _playerStartPosition;
        private readonly Transform _weaponSpawnPosition;
        private readonly List<WeaponSettings> _weapons;

        private CharacterMovement _characterMovementInstance;
        public BootState(CharacterMovement characterMovement, Transform playerStartPosition, Transform weaponSpawnPosition, List<WeaponSettings> weapons)
        {
            _characterMovement = characterMovement;
            _playerStartPosition = playerStartPosition;
            _weaponSpawnPosition = weaponSpawnPosition;
            _weapons = weapons;
        }

        public override void Enter()
        {
            RegisterServices();
            InitializePlayer();
            Services.GetSingle<GameStateMachine>().ChangeStateTo<GameplayState>();
        }

        private void RegisterServices()
        {
            Services.RegisterAsSingle<IInputService>(new PlayerInputService());
            Services.RegisterAsSingle<IRaycastShooter>(new RaycastShooterService());
        }

        private void InitializePlayer()
        {
            var player = GameObject.Instantiate(_characterMovement, _playerStartPosition.position, Quaternion.identity, _playerStartPosition.parent) as CharacterMovement;
            Services.RegisterAsSingle<CharacterMovement>(player);
            player.Initialize(InitializePlayerWeapons());
        }

        private WeaponInventoryProvider InitializePlayerWeapons()
        {
            var playerInventory = new WeaponInventoryProvider();
            foreach (var weapon in _weapons)
            {
                var weaponInstance = GameObject.Instantiate(weapon.WeaponModel, _weaponSpawnPosition);
                playerInventory.Add(weaponInstance);
                weaponInstance.Initialize(weapon, Camera.main);
            }
            playerInventory.EquipWeapon(0);
            return playerInventory;
        }
    }
}