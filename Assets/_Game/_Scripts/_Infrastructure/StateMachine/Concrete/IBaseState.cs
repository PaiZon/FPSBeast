namespace Game.Infrastructure.StateMachine
{
    public interface IBaseState
    {
        void Enter();
        void Tick();
        void FixedTick();
        void Exit();
    }
}
