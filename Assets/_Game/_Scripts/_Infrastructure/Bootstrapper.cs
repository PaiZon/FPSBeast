﻿using System.Collections.Generic;
using Game.Infrastructure.StateMachine;
using Game.Scripts.Infrastructure.Servisces;
using Game.Scripts.Weapon;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Infrastructure
{
    public class Bootstrapper : SerializedMonoBehaviour
    {
        [SerializeField] private CharacterMovement.CharacterMovement _characterMovement;
        [SerializeField] private Transform _playerStartPosition;
        [SerializeField] private Transform _weaponSpawnPosition;
        [SerializeField] private List<WeaponSettings> _weapon;

        private void Awake()
        {
            var gameStateMachine = new GameStateMachine();
            Services.RegisterAsSingle<GameStateMachine>(gameStateMachine);
            gameStateMachine.AddState<BootState>(new BootState(_characterMovement, _playerStartPosition, _weaponSpawnPosition, _weapon));
            gameStateMachine.AddState<GameplayState>(new GameplayState());
            gameStateMachine.ChangeStateTo<BootState>();
        }
    }
}