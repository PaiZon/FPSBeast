﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scripts.Infrastructure.Servisces
{
    public class SceneLoader : ISceneLoader
    {
        private readonly ICoroutineRunner _coroutineRunner;
        
        public SceneLoader(ICoroutineRunner coroutineRunner)
        {
            _coroutineRunner = coroutineRunner;
        }

        public void LoadScene(string sceneToLoad, Action onLoaded = null, LoadSceneMode sceneMode = LoadSceneMode.Single)
        {
            _coroutineRunner.StartCoroutine(LoadSceneInternal(sceneToLoad, onLoaded, sceneMode));
        }
        
        private IEnumerator LoadSceneInternal(string sceneToLoad, Action onLoaded = null, LoadSceneMode sceneMode = LoadSceneMode.Single)
        {
            var asyncOperation = SceneManager.LoadSceneAsync(sceneToLoad, sceneMode);

            yield return new WaitUntil(() => asyncOperation.isDone);
            
            onLoaded?.Invoke();
        }
    }
}