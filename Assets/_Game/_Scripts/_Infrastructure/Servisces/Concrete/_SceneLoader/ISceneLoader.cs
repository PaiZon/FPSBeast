﻿using System;
using UnityEngine.SceneManagement;

namespace Game.Scripts.Infrastructure.Servisces
{
    public interface ISceneLoader : IService
    {
        void LoadScene(string sceneToLoad, Action onLoaded = null, LoadSceneMode sceneMode = LoadSceneMode.Single);
    }
}