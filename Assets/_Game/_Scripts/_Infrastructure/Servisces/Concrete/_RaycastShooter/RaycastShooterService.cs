﻿using UnityEngine;

namespace Game.Scripts.Weapon
{
    public class RaycastShooterService : IRaycastShooter
    {
        public (RaycastHit, bool) ShootRaycast(Ray ray, float distance = float.MaxValue)
        {
            var hitObject = Physics.Raycast(ray, out var hit, distance);
            return (hit, hitObject);
        }
    }
}