﻿using Game.Scripts.Infrastructure.Servisces;
using UnityEngine;

namespace Game.Scripts.Weapon
{
    public interface IRaycastShooter : IService
    {
        (RaycastHit, bool) ShootRaycast(Ray ray, float distance = float.MaxValue);
    }
}