﻿using System.Collections;
using UnityEngine;

namespace Game.Scripts.Infrastructure.Servisces
{
    public interface ICoroutineRunner : IService
    {
        Coroutine StartCoroutine(IEnumerator routine);
    }
}