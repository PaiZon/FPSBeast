﻿using UnityEngine;

namespace Game.Scripts.Infrastructure.Servisces
{
    public class CoroutineRunner : MonoBehaviour, ICoroutineRunner
    {
        private void OnDestroy()
        {
            Services.RemoveSingle<CoroutineRunner>();
        }
    }
}