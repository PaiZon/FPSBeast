﻿using System;
using Game.Scripts.Infrastructure.Servisces;
using UnityEngine.InputSystem;

namespace Game.Scripts.Input
{
    public interface IInputService : IService
    {
        void SetInputSystemState(bool value);
        void AddListener(InputActionType actionType, Action<InputAction.CallbackContext> listener);
        
    }
}