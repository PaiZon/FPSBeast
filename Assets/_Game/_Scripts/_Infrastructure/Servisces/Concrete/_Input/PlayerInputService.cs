using System;
using UnityEngine.InputSystem;

namespace Game.Scripts.Input
{
    
    public class PlayerInputService : IInputService
    {
        private readonly PlayerControls _playerInput;

        public PlayerInputService()
        {
            _playerInput = new PlayerControls();
        }
        
        public void SetInputSystemState(bool value)
        {
            if(value == true)
                _playerInput.Enable();
            else
                _playerInput.Disable();
                
        }

        public void AddListener(InputActionType actionType, Action<InputAction.CallbackContext> listener)
        {
            switch (actionType)
            {
                case InputActionType.WASDMovementPerformed:
                    _playerInput.Movement.WASD.performed += listener;
                    break;
                case InputActionType.LeftMouseButtonStarted:
                    _playerInput.Movement.Fire.started += listener;
                    break;
                case InputActionType.LeftMouseButtonCanceled:
                    _playerInput.Movement.Fire.canceled += listener;
                    break;
                case InputActionType.RButtonStarted:
                    _playerInput.Movement.Reload.started += listener;
                    break;
                case InputActionType.MouseMoveStarted:
                    _playerInput.Movement.Look.started += listener;
                    break;
                case InputActionType.Alpha1Started:
                case InputActionType.Alpha2Started:
                case InputActionType.Alpha3Started:
                case InputActionType.MouseWheelPerformed:
                    _playerInput.Movement.Scroll.performed += listener;
                    break;
                case InputActionType.WASDMovementCanceled:
                    _playerInput.Movement.WASD.canceled += listener;
                    break;
                case InputActionType.MouseMovePerformed:
                    _playerInput.Movement.Look.performed += listener;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(actionType), actionType, null);
            }
        }
    }
}
