﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.Infrastructure.Servisces
{
    public static class Services
    {
        private static readonly Dictionary<Type, object> SERVICES = new Dictionary<Type, object>();

        public static void RegisterAsSingle<TService>(object instance) where TService : IService
        {
            if(SERVICES.ContainsKey(typeof(TService)) == true)
                Debug.LogWarning($"Instance of type {typeof(TService)} already registered");
            SERVICES.Add(typeof(TService), instance);
        }

        public static TService GetSingle<TService>() where TService : IService
        {
            if (IsServiceRegistered<TService>(out var serviceInstance) != false)
                return (TService)serviceInstance;
            
            Debug.LogWarning($"Instance of type {typeof(TService)} does not registered");
            return default;
        }

        public static void RemoveSingle<TService>() where TService : IService
        {
            if(SERVICES.ContainsKey(typeof(TService)) == false)
                Debug.LogWarning($"Instance of type {typeof(TService)} does not registered");
            SERVICES.Remove(typeof(TService));
        }
        
        private static bool IsServiceRegistered<TService>(out object serviceInstance) where TService : IService
        {
            return SERVICES.TryGetValue(typeof(TService), out serviceInstance);
        }
    }
}