﻿using DG.Tweening;
using Game.Scripts.DestroyableObject;
using Game.Scripts.Infrastructure.Servisces;
using Game.Scripts.Input;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using InputActionType = Game.Scripts.Input.InputActionType;

namespace Game.Scripts.Weapon
{
    public class BaseWeapon : MonoBehaviour, IWeapon
    {
        [SerializeField] private SlideBehaviour _slide;
        [SerializeField] private UnityEvent _shotPerformedEvent;
        [SerializeField, FoldoutGroup("Hit Particle Pool")] private ParticleSystem _appropriateObjectHit;
        [SerializeField, FoldoutGroup("Hit Particle Pool")] private int _particleObjectAmount = 10;
        private IRaycastShooter _raycastShooter;
        private Camera _mainCamera;
        private WeaponSettings _settings;
        private int _currentAmmoInClip;
        private bool _shouldShoot = false;
        private bool _isReloading = false;
        private float _delayBetweenShots;
        private float _currentShootingTimer;
        [SerializeField, ReadOnly, FoldoutGroup("Hit Particle Pool", true)] private Pool<ParticleSystem> _hitParticlePool;

        public void Initialize(WeaponSettings settings, Camera mainCamera)
        {
            _hitParticlePool = new Pool<ParticleSystem>(_appropriateObjectHit, _particleObjectAmount, new GameObject($"Hit Particles for Weapon {this.name}").transform, shouldExpand:true);
            _raycastShooter = Services.GetSingle<IRaycastShooter>();
            _settings = settings;
            _mainCamera = mainCamera;
            _currentAmmoInClip = settings.ClipSize;
            Services.GetSingle<IInputService>().AddListener(InputActionType.RButtonStarted, StartReload);
            Services.GetSingle<IInputService>().AddListener(InputActionType.LeftMouseButtonStarted, StartShooting);
            Services.GetSingle<IInputService>().AddListener(InputActionType.LeftMouseButtonCanceled, StartShooting);
            _delayBetweenShots = 1f / settings.FireRate;
        }        
        
        public void Tick()
        {
            if(_shouldShoot == false)
                return;
            if(_currentShootingTimer >= _delayBetweenShots && _isReloading == false)
            {
                if (_currentAmmoInClip <= 0)
                {
                    Reload();
                    return;
                }
                var ray = new Ray(_mainCamera.transform.position, _mainCamera.transform.forward);
                var hitTuple = _raycastShooter.ShootRaycast(ray, _settings.Range < 0 ? float.MaxValue : _settings.Range);
                var isRaycastFoundTarget = hitTuple.Item2;
                var hit = hitTuple.Item1;
                
                _slide.PlayShootAnimation();
                _currentShootingTimer = 0f;
                _currentAmmoInClip -= 1;
                _shotPerformedEvent?.Invoke();
                if(isRaycastFoundTarget == false) return;
                hit.collider.TryGetComponent<IDestroyableObject>(out var destroyableObject);
                if(destroyableObject == null)
                {
                    PlayDefaultHitParticles(hit.point);
                    return;
                }
                if (CanDamageObject(destroyableObject))
                { 
                    destroyableObject.GetHit(_settings.Damage);
                    PlayDefaultHitParticles(hit.point);
                }

            }

            _currentShootingTimer += Time.deltaTime;
        }

        private void PlayDefaultHitParticles(Vector3 hitPoint)
        {
            var instance = _hitParticlePool.GetOrCreate();
            instance.transform.position = hitPoint;
            instance.gameObject.SetActive(true);
        }

        private bool CanDamageObject(IDestroyableObject objectToCheck)
        {
            return _settings.ObjectsThatCanBeDamaged.Contains(objectToCheck.GetObjectMaterial());
        }

        private void OnDisable()
        {
            transform.DOKill();
            _isReloading = false;
            _currentShootingTimer = _delayBetweenShots + 0.1f;
        }

        public Transform GetTransform()
        {
            return transform;
        }

        public void StartShooting(InputAction.CallbackContext callbackContext)
        {
            _shouldShoot = callbackContext.phase == InputActionPhase.Started;
        }

        public void StartReload(InputAction.CallbackContext callbackContext)
        {
            Reload();
        }

        private void Reload()
        {
            if(_isReloading == true)
                return;
            _isReloading = true;
            var originalRotation = transform.localEulerAngles;
            var targetRotation = originalRotation + (Vector3.right * 360f);
            transform.DOLocalRotate(targetRotation,
                _settings.ReloadTime, RotateMode.FastBeyond360).OnComplete(() =>
            {
                _currentAmmoInClip = _settings.ClipSize;
                _currentShootingTimer = _delayBetweenShots + 0.1f;
                _isReloading = false;
            });
        }
    }
}