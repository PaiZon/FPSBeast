﻿using System.Collections.Generic;
using Game.Scripts.DestroyableObject;
using UnityEngine;

namespace Game.Scripts.Weapon
{
    [CreateAssetMenu(menuName = "FPS/Weapon Settings", fileName = "new Weapon Settings")]
    public class WeaponSettings : ScriptableObject
    {
        [SerializeField] private BaseWeapon _weaponModel;
        [SerializeField] private int _damage;
        [SerializeField, Tooltip("-1 for unlimited Range")] private float _range;
        [SerializeField] private int _clipSize;
        [SerializeField, Tooltip("Bullets/Per second")] private int _fireRate;
        [SerializeField] private float _reloadTime;
        [SerializeField] private List<ObjectMaterial> _objectsThatCanBeDamaged;

        public BaseWeapon WeaponModel => _weaponModel;
        public int Damage => _damage;
        public float Range => _range;
        public int ClipSize => _clipSize;
        public int FireRate => _fireRate;
        public float ReloadTime => _reloadTime;
        public List<ObjectMaterial> ObjectsThatCanBeDamaged => _objectsThatCanBeDamaged;
    }
}