using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Scripts.Weapon
{
    public interface IWeapon
    {
        void Tick();
        void StartShooting(InputAction.CallbackContext callbackContext);
        void StartReload(InputAction.CallbackContext callbackContext);
        void Initialize(WeaponSettings settings, Camera mainCamera);
        Transform GetTransform();
    }
}
