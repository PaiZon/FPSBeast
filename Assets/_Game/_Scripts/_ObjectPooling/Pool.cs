using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[System.Serializable]
public class Pool<T> where T : Component
{
    public readonly List<T> _pool;
    public readonly T _objectToPool;
    public readonly Transform _parent;
    public readonly bool _shouldExpand = false;
    
    
    public Pool(T objectToPool, int amountInPool, Transform parent = null, bool shouldExpand = false)
    {
        _pool = new List<T>();
        _objectToPool = objectToPool;
        _parent = parent;
        _shouldExpand = shouldExpand;
        SpawnObjects(objectToPool, amountInPool);
    }

    private void SpawnObjects(T objectToPool, int amountInPool)
    {
        for (int i = 0; i < amountInPool; i++)
        {
            SpawnObject(objectToPool);
        }
    }

    private T SpawnObject(T objectToPool)
    {
        var instance = GameObject.Instantiate(objectToPool, _parent);
        instance.gameObject.SetActive(false);
        _pool.Add(instance);
        return instance;
    }

    public T GetOrCreate()
    {
        T freeInstance = _pool.First((x) => x.gameObject.activeInHierarchy == false);
        
        if (freeInstance == null || _shouldExpand == false) 
            return null;

        return SpawnObject(_objectToPool);
    }
}